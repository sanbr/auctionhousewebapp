Auction House web application is built using Spring Boot framework and JDK 8

To test this application:
1- Run maven build: mvnw clean package
2- Run using Java 8: java -jar target/auction-house-0.0.1-SNAPSHOT.jar
3- The API is accessible on 8080 port
3- You can access Open API documentation (Swagger) using fhe following link
   http://localhost:8080/swagger-ui/index.html?configUrl=/auction-house-api/swagger-config

