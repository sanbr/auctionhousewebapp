package com.auction.house.data.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AuctionStatusTest {

    @Test
    void parse_not_started_status(){
        Assertions.assertEquals(AuctionStatus.NOT_STARTED, AuctionStatus.from("not_started"));
    }

    @Test
    void parse_running_status(){
        Assertions.assertEquals(AuctionStatus.RUNNING, AuctionStatus.from("running"));
    }

    @Test
    void parse_terminated_status(){
        Assertions.assertEquals(AuctionStatus.TERMINATED, AuctionStatus.from("terminated"));
    }

    @Test
    void parse_deleted_status(){
        Assertions.assertEquals(AuctionStatus.DELETED, AuctionStatus.from("deleted"));
    }

    @Test
    void parse_invalid_status(){
        Assertions.assertEquals(AuctionStatus.UNKNOWN, AuctionStatus.from("not_valid123"));
    }
}