package com.auction.house.controller;

import com.auction.house.data.entity.Auction;
import com.auction.house.data.entity.Bid;
import com.auction.house.data.repository.AuctionRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = BidController.class)
class BidControllerTest {
    private static final String AUCTION_NAME = "TestAuction";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private AuctionRepository auctionRepository;

    @Test
    void post_valid_bid_on_auction_should_return_status_created() throws Exception{
        Auction auction=new Auction();
        auction.setName(AUCTION_NAME);
        auction.setCurrentPrice(50);
        auction.setStartingTime(Date.from(Instant.EPOCH));
        auction.setEndingTime(new Date(Instant.now().getEpochSecond() * 1000 + 8000));

        Bid bid=new Bid();
        bid.setUsername("bidder1");
        bid.setPrice(60);

        Mockito.when(auctionRepository.findById(AUCTION_NAME)).thenReturn(Optional.of(auction));

        mockMvc.perform(post("/auction/{auctionName}/bid", AUCTION_NAME)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(bid)))
                .andExpect(status().isCreated());

        Assertions.assertTrue(auction.getBidList().contains(bid));
        Assertions.assertEquals(bid.getPrice(),auction.getCurrentPrice());
        Mockito.verify(auctionRepository).save(Mockito.eq(auction));

    }

    @Test
    void post_bid_lower_than_auction_current_price_should_return_status_bad_request() throws Exception {
        Auction auction=new Auction();
        auction.setName(AUCTION_NAME);
        auction.setCurrentPrice(50);

        Bid bid=new Bid();
        bid.setUsername("bidder1");
        bid.setPrice(20);

        Mockito.when(auctionRepository.findById(AUCTION_NAME)).thenReturn(Optional.of(auction));

        mockMvc.perform(post("/auction/{auctionName}/bid", AUCTION_NAME)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(bid)))
                .andExpect(status().isBadRequest());

    }
    @Test
    void post_bid_with_empty_user_name_should_return_status_length_required() throws Exception {
        Bid bid=new Bid();
        bid.setUsername("");


        mockMvc.perform(post("/auction/{auctionName}/bid", AUCTION_NAME)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(bid)))
                .andExpect(status().isLengthRequired());

    }

    @Test
    void post_bid_with_auction_not_found_should_return_status_not_found() throws Exception {
        Bid bid=new Bid();
        bid.setUsername("bidder1");

        Mockito.when(auctionRepository.findById(AUCTION_NAME)).thenReturn(Optional.empty());
        mockMvc.perform(post("/auction/{auctionName}/bid", AUCTION_NAME)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(bid)))
                .andExpect(status().isNotFound());

    }

    @Test
    void post_bid_on_terminated_auction_should_return_status_unauthorized() throws Exception{
        Auction auction=new Auction();
        auction.setName(AUCTION_NAME);
        auction.setCurrentPrice(50);
        auction.setStartingTime(new Date(Instant.now().getEpochSecond() * 1000 + 8000));

        Bid bid=new Bid();
        bid.setUsername("bidder1");
        bid.setPrice(60);

        Mockito.when(auctionRepository.findById(AUCTION_NAME)).thenReturn(Optional.of(auction));

        mockMvc.perform(post("/auction/{auctionName}/bid", AUCTION_NAME)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(bid)))
                .andExpect(status().isUnauthorized());


    }
    @Test
    void post_bid_on_not_started_auction_should_return_status_forbidden() throws Exception{
        Auction auction=new Auction();
        auction.setName(AUCTION_NAME);
        auction.setCurrentPrice(50);
        auction.setStartingTime(Date.from(Instant.EPOCH));
        auction.setEndingTime(new Date(Instant.now().getEpochSecond() * 1000 - 8000));

        Bid bid=new Bid();
        bid.setUsername("bidder1");
        bid.setPrice(60);

        Mockito.when(auctionRepository.findById(AUCTION_NAME)).thenReturn(Optional.of(auction));

        mockMvc.perform(post("/auction/{auctionName}/bid", AUCTION_NAME)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(bid)))
                .andExpect(status().isForbidden());


    }

    @Test
    void get_all_bids() throws Exception{
        Auction auction=new Auction();
        auction.setName(AUCTION_NAME);
        auction.setCurrentPrice(50);
        auction.setStartingTime(Date.from(Instant.EPOCH));
        auction.setEndingTime(new Date(Instant.now().getEpochSecond() * 1000 + 8000));

        Bid bid1=new Bid();
        bid1.setUsername("bidder1");
        bid1.setPrice(60);

        Bid bid2=new Bid();
        bid2.setUsername("bidder2");
        bid2.setPrice(60);

        auction.getBidList().addAll(Arrays.asList(bid1,bid2));

        Mockito.when(auctionRepository.findById(AUCTION_NAME)).thenReturn(Optional.of(auction));

        mockMvc.perform(get("/auction/{auctionName}/bid/all", AUCTION_NAME)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(auction.getBidList())));

    }
    @Test
    void get_all_bids_with_auction_not_existing_should_return_not_found() throws Exception{

        Mockito.when(auctionRepository.findById(AUCTION_NAME)).thenReturn(Optional.empty());

        mockMvc.perform(get("/auction/{auctionName}/bid/all", AUCTION_NAME)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    void get_auction_winner() throws Exception{
        Auction auction=new Auction();
        auction.setName(AUCTION_NAME);
        auction.setCurrentPrice(60);
        auction.setStartingTime(Date.from(Instant.EPOCH));
        auction.setEndingTime(new Date(Instant.now().getEpochSecond() * 1000-8000));

        Bid bid1=new Bid();
        bid1.setUsername("bidder1");
        bid1.setPrice(60);

        Bid bid2=new Bid();
        bid2.setUsername("bidder2");
        bid2.setPrice(55);

        auction.getBidList().addAll(Arrays.asList(bid1,bid2));

        Mockito.when(auctionRepository.findById(AUCTION_NAME)).thenReturn(Optional.of(auction));

        mockMvc.perform(get("/auction/{auctionName}/bid/winner", AUCTION_NAME)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(bid1)));

    }

    @Test
    void get_auction_winner_when_no_bid_found_should_return_status_no_content() throws Exception{
        Auction auction=new Auction();
        auction.setName(AUCTION_NAME);
        auction.setCurrentPrice(60);
        auction.setStartingTime(Date.from(Instant.EPOCH));
        auction.setEndingTime(new Date(Instant.now().getEpochSecond() * 1000-8000));

        Mockito.when(auctionRepository.findById(AUCTION_NAME)).thenReturn(Optional.of(auction));

        mockMvc.perform(get("/auction/{auctionName}/bid/winner", AUCTION_NAME)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

    }

    @Test
    void get_auction_winner_with_action_not_existing_should_return_status_no_found() throws Exception{
        Mockito.when(auctionRepository.findById(AUCTION_NAME)).thenReturn(Optional.empty());

        mockMvc.perform(get("/auction/{auctionName}/bid/winner", AUCTION_NAME)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    void get_auction_winner_when_auction_not_terminated_should_return_status_bad_request() throws Exception{
        Auction auction=new Auction();
        auction.setName(AUCTION_NAME);
        auction.setCurrentPrice(60);
        auction.setStartingTime(Date.from(Instant.EPOCH));
        auction.setEndingTime(new Date(Instant.now().getEpochSecond() * 1000 + 8000));

        Mockito.when(auctionRepository.findById(AUCTION_NAME)).thenReturn(Optional.of(auction));

        mockMvc.perform(get("/auction/{auctionName}/bid/winner", AUCTION_NAME)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }
}