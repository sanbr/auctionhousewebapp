package com.auction.house.controller;

import com.auction.house.data.entity.Auction;
import com.auction.house.data.entity.AuctionHouse;
import com.auction.house.data.repository.AuctionHouseRepository;
import com.auction.house.data.repository.AuctionRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = AuctionController.class)
public class AuctionControllerTest {
    private static final String AUCTION_HOUSE_NAME = "TestAuctionHouse";
    private static final String AUCTION_NAME = "TestAuction";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private AuctionHouseRepository auctionHouseRepository;

    @MockBean
    private AuctionRepository auctionRepository;

    @Test
    void post_valid_auction_house_should_returns_status_created() throws Exception {
        AuctionHouse auctionHouse = new AuctionHouse();
        auctionHouse.setName(AUCTION_HOUSE_NAME);
        Auction auction = new Auction();
        auction.setName(AUCTION_NAME);

        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.of(auctionHouse));

        mockMvc.perform(post("/auction-house/{auctionHouseName}/auction", AUCTION_HOUSE_NAME)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(auction)))
                .andExpect(status().isCreated());

        Assertions.assertTrue(auctionHouse.getAuctionList().contains(auction));

        Mockito.verify(auctionHouseRepository).findById(Mockito.eq(AUCTION_HOUSE_NAME));
        Mockito.verify(auctionHouseRepository).save(Mockito.eq(auctionHouse));
    }

    @Test
    void post_with_empty_auction_name_should_returns_error_status_bad_request() throws Exception {
        AuctionHouse auctionHouse = new AuctionHouse();
        auctionHouse.setName(AUCTION_HOUSE_NAME);
        Auction auction = new Auction();
        auction.setName(" ");

        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.of(auctionHouse));

        mockMvc.perform(post("/auction-house/{auctionHouseName}/auction", AUCTION_HOUSE_NAME)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(auction)))
                .andExpect(status().isBadRequest());

        Mockito.verify(auctionHouseRepository, Mockito.never()).save(Mockito.any());
    }

    @Test
    void post_with_non_existing_auction_house_name_should_returns_error_status_not_found() throws Exception {
        Auction auction = new Auction();
        auction.setName(AUCTION_NAME);

        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.empty());

        mockMvc.perform(post("/auction-house/{auctionHouseName}/auction", AUCTION_HOUSE_NAME)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(auction)))
                .andExpect(status().isNotFound());

        Mockito.verify(auctionHouseRepository, Mockito.never()).save(Mockito.any());
    }

    @Test
    void post_with_existing_auction_name_should_returns_error_status_conflict() throws Exception {
        AuctionHouse auctionHouse = new AuctionHouse();
        auctionHouse.setName(AUCTION_HOUSE_NAME);
        Auction auction = new Auction();
        auction.setName(AUCTION_NAME);

        auctionHouse.getAuctionList().add(auction);

        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.of(auctionHouse));

        mockMvc.perform(post("/auction-house/{auctionHouseName}/auction", AUCTION_HOUSE_NAME)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(auction)))
                .andExpect(status().isConflict());

        Mockito.verify(auctionHouseRepository, Mockito.never()).save(Mockito.any());
    }

    @Test
    void delete_existing_auction_should_returns_status_ok() throws Exception {
        AuctionHouse auctionHouse = new AuctionHouse();
        auctionHouse.setName(AUCTION_HOUSE_NAME);
        Auction auction = new Auction();
        auction.setName(AUCTION_NAME);

        auctionHouse.getAuctionList().add(auction);

        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.of(auctionHouse));

        Assertions.assertFalse(auction.isDeleted());

        mockMvc.perform(delete("/auction-house/{auctionHouseName}/auction/{auction-name}", AUCTION_HOUSE_NAME, AUCTION_NAME))
               .andExpect(status().isOk());

        Assertions.assertTrue(auction.isDeleted());

        Mockito.verify(auctionRepository).save(auction);
    }

    @Test
    void delete_non_existing_auction_should_returns_status_not_found() throws Exception {
        AuctionHouse auctionHouse = new AuctionHouse();
        auctionHouse.setName(AUCTION_HOUSE_NAME);

        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.of(auctionHouse));

        mockMvc.perform(delete("/auction-house/{auctionHouseName}/auction/{auction-name}", AUCTION_HOUSE_NAME, AUCTION_NAME))
                .andExpect(status().isNotFound());

        Mockito.verify(auctionRepository, Mockito.never()).save(Mockito.any());
    }

    @Test
    void delete_with_non_existing_auction_house_should_returns_status_not_found() throws Exception {
        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.empty());

        mockMvc.perform(delete("/auction-house/{auctionHouseName}/auction/{auction-name}", AUCTION_HOUSE_NAME, AUCTION_NAME))
                .andExpect(status().isNotFound());

        Mockito.verify(auctionRepository, Mockito.never()).save(Mockito.any());
    }


    @Test
    void get_all_auctions() throws Exception {
        AuctionHouse auctionHouse = new AuctionHouse();
        auctionHouse.setName(AUCTION_HOUSE_NAME);
        Auction auction1 = new Auction();
        auction1.setName(AUCTION_NAME);
        Auction auction2 = new Auction();
        auction2.setName(AUCTION_NAME + 2);

        auctionHouse.getAuctionList().addAll(Arrays.asList(auction1, auction2));

        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.of(auctionHouse));

        MvcResult mvcResult = mockMvc.perform(get("/auction-house/{auctionHouseName}/auction/all", AUCTION_HOUSE_NAME)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        List<Auction> auctions = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<Auction>>() {});

        Assertions.assertEquals(2, auctions.size());
        Assertions.assertTrue(auctions.contains(auction1));
        Assertions.assertTrue(auctions.contains(auction2));

        Mockito.verify(auctionHouseRepository).findById(AUCTION_HOUSE_NAME);
    }

    @Test
    void get_all_deleted_auctions() throws Exception {
        AuctionHouse auctionHouse = new AuctionHouse();
        auctionHouse.setName(AUCTION_HOUSE_NAME);
        Auction auction1 = new Auction();
        auction1.setName(AUCTION_NAME);
        Auction auction2 = new Auction();
        auction2.setName(AUCTION_NAME + 2);
        auction2.setDeleted(true);

        auctionHouse.getAuctionList().addAll(Arrays.asList(auction1, auction2));

        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.of(auctionHouse));

        MvcResult mvcResult = mockMvc.perform(get("/auction-house/{auctionHouseName}/auction/all", AUCTION_HOUSE_NAME)
                .accept(MediaType.APPLICATION_JSON)
                .queryParam("status", "deleted"))
                .andExpect(status().isOk())
                .andReturn();
        List<Auction> auctions = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<Auction>>() {});

        Assertions.assertEquals(1, auctions.size());
        Assertions.assertEquals(auction2, auctions.iterator().next());

        Mockito.verify(auctionHouseRepository).findById(AUCTION_HOUSE_NAME);
    }


    @Test
    void get_all_not_started_auctions() throws Exception {
        AuctionHouse auctionHouse = new AuctionHouse();
        auctionHouse.setName(AUCTION_HOUSE_NAME);
        Auction auction1 = new Auction();
        auction1.setName(AUCTION_NAME);
        auction1.setStartingTime(Date.from(Instant.EPOCH));
        Auction auction2 = new Auction();
        auction2.setName(AUCTION_NAME + 2);
        auction2.setStartingTime(new Date(Instant.now().getEpochSecond() * 1000 + 8000));

        auctionHouse.getAuctionList().addAll(Arrays.asList(auction1, auction2));

        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.of(auctionHouse));

        MvcResult mvcResult = mockMvc.perform(get("/auction-house/{auctionHouseName}/auction/all", AUCTION_HOUSE_NAME)
                .accept(MediaType.APPLICATION_JSON)
                .queryParam("status", "not_started"))
                .andExpect(status().isOk())
                .andReturn();
        List<Auction> auctions = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<Auction>>() {});

        Assertions.assertEquals(1, auctions.size());
        Assertions.assertEquals(auction2, auctions.iterator().next());

        Mockito.verify(auctionHouseRepository).findById(AUCTION_HOUSE_NAME);
    }

    @Test
    void get_all_terminated_auctions() throws Exception {
        AuctionHouse auctionHouse = new AuctionHouse();
        auctionHouse.setName(AUCTION_HOUSE_NAME);
        Auction auction1= new Auction();
        auction1.setName(AUCTION_NAME);
        auction1.setEndingTime(Date.from(Instant.EPOCH));
        Auction auction2 = new Auction();
        auction2.setName(AUCTION_NAME + 2);
        auction2.setEndingTime(new Date(Instant.now().getEpochSecond() * 1000 + 8000));

        auctionHouse.getAuctionList().addAll(Arrays.asList(auction1, auction2));

        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.of(auctionHouse));

        MvcResult mvcResult = mockMvc.perform(get("/auction-house/{auctionHouseName}/auction/all", AUCTION_HOUSE_NAME)
                .accept(MediaType.APPLICATION_JSON)
                .queryParam("status", "terminated"))
                .andExpect(status().isOk())
                .andReturn();
        List<Auction> auctions = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<Auction>>() {});

        Assertions.assertEquals(1, auctions.size());
        Assertions.assertEquals(auction1, auctions.iterator().next());

        Mockito.verify(auctionHouseRepository).findById(AUCTION_HOUSE_NAME);
    }

    @Test
    void get_all_running_auctions() throws Exception {
        AuctionHouse auctionHouse = new AuctionHouse();
        auctionHouse.setName(AUCTION_HOUSE_NAME);
        Auction auction1= new Auction();
        auction1.setName(AUCTION_NAME);
        auction1.setStartingTime(Date.from(Instant.EPOCH));
        auction1.setEndingTime(new Date(Instant.now().getEpochSecond() * 1000 + 8000));
        Auction auction2 = new Auction();
        auction2.setName(AUCTION_NAME + 2);
        auction2.setStartingTime(new Date(Instant.now().getEpochSecond() * 1000 + 8000));
        Auction auction3 = new Auction();
        auction3.setName(AUCTION_NAME + 3);
        auction3.setStartingTime(Date.from(Instant.EPOCH));
        auction3.setEndingTime(new Date(Instant.now().getEpochSecond() * 1000 - 8000));


        auctionHouse.getAuctionList().addAll(Arrays.asList(auction1, auction2,auction3));

        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.of(auctionHouse));

        MvcResult mvcResult = mockMvc.perform(get("/auction-house/{auctionHouseName}/auction/all", AUCTION_HOUSE_NAME)
                .accept(MediaType.APPLICATION_JSON)
                .queryParam("status", "running"))
                .andExpect(status().isOk())
                .andReturn();
        List<Auction> auctions = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<Auction>>() {});

        Assertions.assertEquals(1, auctions.size());
        Assertions.assertEquals(auction1, auctions.iterator().next());

        Mockito.verify(auctionHouseRepository).findById(AUCTION_HOUSE_NAME);
    }

}
