package com.auction.house.controller;

import com.auction.house.data.entity.AuctionHouse;
import com.auction.house.data.repository.AuctionHouseRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = AuctionHouseController.class)
class AuctionHouseControllerTest {
    private static final String AUCTION_HOUSE_NAME = "TestAuctionHouse";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private AuctionHouseRepository auctionHouseRepository;

    @Test
    void post_valid_auction_house_should_returns_status_created() throws Exception {
        AuctionHouse auctionHouse = new AuctionHouse();
        auctionHouse.setName(AUCTION_HOUSE_NAME);

        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.empty());

        mockMvc.perform(post("/auction-house")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(auctionHouse)))
                .andExpect(status().isCreated());

        Mockito.verify(auctionHouseRepository).findById(Mockito.eq(AUCTION_HOUSE_NAME));
        Mockito.verify(auctionHouseRepository).save(Mockito.eq(auctionHouse));
    }

    @Test
    void post_with_empty_auction_house_name_should_returns_error_status_bad_request() throws Exception {
        AuctionHouse auctionHouse = new AuctionHouse();
        auctionHouse.setName("");

        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.empty());

        mockMvc.perform(post("/auction-house")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(auctionHouse)))
                .andExpect(status().isBadRequest());

        Mockito.verify(auctionHouseRepository, Mockito.never()).save(Mockito.any());
    }

    @Test
    void post_with_existing_auction_house_should_returns_error_status_conflict() throws Exception {
        AuctionHouse auctionHouse = new AuctionHouse();
        auctionHouse.setName(AUCTION_HOUSE_NAME);

        Mockito.when(auctionHouseRepository.findById(AUCTION_HOUSE_NAME)).thenReturn(Optional.of(auctionHouse));

        mockMvc.perform(post("/auction-house")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(auctionHouse)))
                .andExpect(status().isConflict());

        Mockito.verify(auctionHouseRepository, Mockito.never()).save(Mockito.any());
    }

    @Test
    void get_all_auction_houses() throws Exception {
        AuctionHouse auctionHouse1 = new AuctionHouse();
        auctionHouse1.setName(AUCTION_HOUSE_NAME);
        AuctionHouse auctionHouse2 = new AuctionHouse();
        auctionHouse2.setName(AUCTION_HOUSE_NAME + 2);

        Mockito.when(auctionHouseRepository.findAll()).thenReturn(Arrays.asList(auctionHouse1, auctionHouse2));

        MvcResult mvcResult = mockMvc.perform(get("/auction-house/all").accept(MediaType.APPLICATION_JSON))
                                      .andExpect(status().isOk())
                                      .andReturn();
        List<AuctionHouse> auctionHouses = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), new TypeReference<List<AuctionHouse>>() {});

        Assertions.assertEquals(2, auctionHouses.size());
        Assertions.assertTrue(auctionHouses.contains(auctionHouse1));
        Assertions.assertTrue(auctionHouses.contains(auctionHouse2));

        Mockito.verify(auctionHouseRepository).findAll();
    }

    @Test
    void delete_existing_auction_house_should_returns_status_ok() throws Exception {
        mockMvc.perform(delete("/auction-house/{auctionHouseName}", AUCTION_HOUSE_NAME))
                .andExpect(status().isOk());

        Mockito.verify(auctionHouseRepository).deleteById(Mockito.eq(AUCTION_HOUSE_NAME));
    }

    @Test
    void delete_non_existing_auction_house_should_returns_status_not_found() throws Exception {
        Mockito.doThrow(new EmptyResultDataAccessException(1)).when(auctionHouseRepository).deleteById(AUCTION_HOUSE_NAME);

        mockMvc.perform(delete("/auction-house/{auctionHouseName}", AUCTION_HOUSE_NAME))
                .andExpect(status().isNotFound());

        Mockito.verify(auctionHouseRepository).deleteById(Mockito.eq(AUCTION_HOUSE_NAME));
    }
}
