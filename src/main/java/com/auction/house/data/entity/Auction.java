package com.auction.house.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.Hidden;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Instance of this class represents an auction
 */
@Entity
public class Auction {
    @Id
    @Column
    private String name;
    @Column
    private String description;
    @Column(nullable = false)
    private Date startingTime;
    @Column(nullable = false)
    private Date endingTime;
    @Column(nullable = false)
    private int startPrice;
    @Hidden
    @Column(nullable = false)
    private int currentPrice;
    @JsonIgnore
    @Column(nullable = false)
    private boolean deleted = false;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL)
    @Column
    private List<Bid> bidList = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(Date startingTime) {
        this.startingTime = startingTime;
    }

    public Date getEndingTime() {
        return endingTime;
    }

    public void setEndingTime(Date endingTime) {
        this.endingTime = endingTime;
    }

    public int getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(int startPrice) {
        this.startPrice = startPrice;
    }

    public int getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(int currentPrice) {
        this.currentPrice = currentPrice;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<Bid> getBidList() {
        return bidList;
    }

    public void setBidList(List<Bid> bidList) {
        this.bidList = bidList;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other){
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Auction auction = (Auction) other;
        return Objects.equals(name, auction.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
