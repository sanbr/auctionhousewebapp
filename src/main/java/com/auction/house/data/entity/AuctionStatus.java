package com.auction.house.data.entity;

/**
 * This enum represents the different status of an auction
 */
public enum AuctionStatus {
    NOT_STARTED,
    RUNNING,
    TERMINATED,
    DELETED,
    UNKNOWN;

    public static AuctionStatus from(String status){
        switch (status){
            case "not_started":
                return NOT_STARTED;
            case "running":
                return RUNNING;
            case "terminated":
                return TERMINATED;
            case "deleted":
                return DELETED;
            default:
                return UNKNOWN;
        }
    }
}
