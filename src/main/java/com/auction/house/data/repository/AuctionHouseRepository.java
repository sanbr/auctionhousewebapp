package com.auction.house.data.repository;

import com.auction.house.data.entity.AuctionHouse;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuctionHouseRepository extends CrudRepository<AuctionHouse,String> {
}
