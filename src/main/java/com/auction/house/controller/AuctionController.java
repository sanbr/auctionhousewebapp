package com.auction.house.controller;

import com.auction.house.data.entity.Auction;
import com.auction.house.data.entity.AuctionHouse;
import com.auction.house.data.entity.AuctionStatus;
import com.auction.house.data.repository.AuctionHouseRepository;
import com.auction.house.data.repository.AuctionRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/auction-house/{auctionHouseName}/auction", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Auction endpoint", description = "Use this endpoint to manage an auction taking place in an auction house")
public class AuctionController {
    private final AuctionRepository auctionRepository;
    private final AuctionHouseRepository auctionHouseRepository;

    public AuctionController(AuctionRepository auctionRepository, AuctionHouseRepository auctionHouseRepository) {
        this.auctionRepository = auctionRepository;
        this.auctionHouseRepository = auctionHouseRepository;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary="Add new action to an auction house")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Auction created successfully",
                    content = {
                            @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = Auction.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Auction house not found"),
            @ApiResponse(responseCode = "400", description = "Auction name is empty"),
            @ApiResponse(responseCode = "409", description = "Auction already exists"),
            @ApiResponse(responseCode = "500", description = "Unable to create the auction")
    })
    public void createAuction(@PathVariable String auctionHouseName, @RequestBody Auction auction){
        Optional<AuctionHouse> auctionHouseOptional = auctionHouseRepository.findById(auctionHouseName);
        if(!auctionHouseOptional.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Auction house [%s] not found", auctionHouseName));
        }
        String auctionName = auction.getName();
        if(auctionName.trim().isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Auction name is empty");
        }
        AuctionHouse auctionHouse = auctionHouseOptional.get();
        List<Auction> auctionList = auctionHouse.getAuctionList();
        if(auctionList.contains(auction)){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Auction already exists");
        }
        // The first current price is the starting price
        auction.setCurrentPrice(auction.getStartPrice());
        auctionList.add(auction);
        auctionHouseRepository.save(auctionHouse);
    }

    @Operation(summary="Get all auctions taking place in an auction house")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned actions list with success",
                    content = {
                        @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(allOf = Auction.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Auction house not found"),
            @ApiResponse(responseCode = "400", description = "Invalid auction status")
    })
    @GetMapping("/all")
    public List<Auction> listAllAuctions(@PathVariable String auctionHouseName, @RequestParam(required = false) String status){
        Optional<AuctionHouse> auctionHouseOptional = auctionHouseRepository.findById(auctionHouseName);
        if(!auctionHouseOptional.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Auction house [%s] not found", auctionHouseName));
        }
        List<Auction> auctionList = auctionHouseOptional.get().getAuctionList();
        if(status == null){
            return auctionList;
        }
        switch (AuctionStatus.from(status.trim().toLowerCase())){
            case NOT_STARTED:
                return auctionList.stream()
                                   .filter(auction -> Date.from(Instant.now()).before(auction.getStartingTime()))
                                   .collect(Collectors.toList());
            case TERMINATED:
                return auctionList.stream()
                                .filter(auction -> Date.from(Instant.now()).after(auction.getEndingTime()))
                                .collect(Collectors.toList());
            case RUNNING:
                Date now = Date.from(Instant.now());
                return auctionList.stream()
                                .filter(auction -> now.after(auction.getStartingTime()) && now.before(auction.getEndingTime()))
                                .collect(Collectors.toList());
            case DELETED:
                return auctionList.stream()
                                .filter(Auction::isDeleted)
                                .collect(Collectors.toList());
            case UNKNOWN:
            default:
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Invalid auction status [%s]", status));
        }
    }

    @DeleteMapping("/{auction-name}")
    @Operation(summary="Delete an auction from an auction house")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Auction deleted with success"),
            @ApiResponse(responseCode = "404", description = "Auction house not found"),
            @ApiResponse(responseCode = "404", description = "Auction not found")
    })
    public void deleteAuction(@PathVariable String auctionHouseName, @PathVariable("auction-name") String auctionName){
        Optional<AuctionHouse> auctionHouseOptional = auctionHouseRepository.findById(auctionHouseName);
        if(!auctionHouseOptional.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Auction house [%s] not found", auctionHouseName));
        }
        Optional<Auction> auctionOptional=auctionHouseOptional.get().getAuctionList().stream()
                .filter(auction->auctionName.equals(auction.getName()))
                .findAny();
        if(!auctionOptional.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Auction [%s] not found", auctionName));
        }
        // Perform a logical delete in order to manage deleted auction status
        auctionOptional.get().setDeleted(true);
        auctionRepository.save(auctionOptional.get());
    }

}
