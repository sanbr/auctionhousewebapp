package com.auction.house.controller;


import com.auction.house.data.entity.Auction;
import com.auction.house.data.entity.Bid;
import com.auction.house.data.repository.AuctionRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/auction/{auctionName}/bid", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Bid endpoint", description = "Use this endpoint to manage bidding in an auction")
public class BidController {
    private final AuctionRepository auctionRepository;

    public BidController(AuctionRepository auctionRepository) {
        this.auctionRepository = auctionRepository;
    }

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary="Receive user bid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "User bid accepted",
                    content = {
                            @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = Bid.class))
                    }),
            @ApiResponse(responseCode = "411", description = "User name is empty"),
            @ApiResponse(responseCode = "404", description = "Auction not found"),
            @ApiResponse(responseCode = "400", description = "Bid price is lower than the auction current price"),
            @ApiResponse(responseCode = "401", description = "Auction not started yet"),
            @ApiResponse(responseCode = "403", description = "Auction is already terminated")
    })
    public Bid bidOnAuction( @PathVariable String auctionName, @RequestBody Bid bid){
        if(bid.getUsername().trim().isEmpty()){
            throw new ResponseStatusException(HttpStatus.LENGTH_REQUIRED, "User name is empty");
        }
        Optional<Auction> optional=auctionRepository.findById(auctionName);
        if(!optional.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Auction [%s] not found", auctionName));
        }

        Auction auction =optional.get();

        if(auction.isDeleted()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Auction [%s] not found", auctionName));
        }
        if(auction.getCurrentPrice()>bid.getPrice()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bid price is lower than the auction current price");
        }
        Date now = Date.from(Instant.now());
        if(now.before(auction.getStartingTime())){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Auction not started yet");
        }
        if(now.after(auction.getEndingTime())){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Auction is already terminated");
        }

        auction.getBidList().add(bid);
        auction.setCurrentPrice(bid.getPrice());
        auctionRepository.save(auction);
        return bid;
    }

    @GetMapping("/all")
    @Operation(summary="Get all auction's bids")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned action's bid list with success",
                    content = {
                            @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(allOf = Bid.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Auction not found")
    })
    public List<Bid> getAllBids(@PathVariable String auctionName){
        Optional<Auction> optional=auctionRepository.findById(auctionName);
        if(!optional.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Auction [%s] not found", auctionName));
        }
        Auction auction =optional.get();
        return auction.getBidList();
    }

    @GetMapping("/winner")
    @Operation(summary="Get auction's winner bid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Retuned the winner bid",
                    content = {
                            @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = Bid.class))
                    }),
            @ApiResponse(responseCode = "204", description = "Unfortunately no one bid on the auction"),
            @ApiResponse(responseCode = "404", description = "Auction not found"),
            @ApiResponse(responseCode = "400", description = "Auction is not terminated yet")
    })
    public ResponseEntity<Object> getAuctionWinner(@PathVariable String auctionName){
        Optional<Auction> optional=auctionRepository.findById(auctionName);
        if(!optional.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.format("Auction [%s] not found", auctionName));
        }
        Auction auction =optional.get();
        if(Date.from(Instant.now()).before(auction.getEndingTime())){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Auction is not terminated yet");
        }
        Optional<Bid> auctionWinnerOptional=auction.getBidList().
                stream().filter(bid->bid.getPrice()==auction.getCurrentPrice()).findFirst();
        if(!auctionWinnerOptional.isPresent()){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Unfortunately no one bid on the auction");
        }
        return ResponseEntity.ok(auctionWinnerOptional.get());
    }

}
