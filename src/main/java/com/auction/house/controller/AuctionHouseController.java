package com.auction.house.controller;

import com.auction.house.data.entity.AuctionHouse;
import com.auction.house.data.repository.AuctionHouseRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping(value = "/auction-house", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Auction house endpoint", description = "Use this endpoint to manage an auction house")
public class AuctionHouseController {
    private final AuctionHouseRepository auctionHouseRepository;

    public AuctionHouseController(AuctionHouseRepository auctionHouseRepository) {
        this.auctionHouseRepository = auctionHouseRepository;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary="Create new auction house")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Auction house created successfully",
                    content = {
                            @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AuctionHouse.class))
                    }),
            @ApiResponse(responseCode = "400", description = "Auction house name is empty"),
            @ApiResponse(responseCode = "409", description = "Auction house already exists"),
            @ApiResponse(responseCode = "500", description = "Unable to create the auction house")
    })
    public AuctionHouse createAuctionHouse(@RequestBody AuctionHouse auctionHouse){
        String auctionHouseName = auctionHouse.getName();
        if(auctionHouseName.trim().isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Auction house name is empty");
        }
        if(auctionHouseRepository.findById(auctionHouseName).isPresent()){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Auction house already exists");
        }
        return auctionHouseRepository.save(auctionHouse);
    }

    @GetMapping("/all")
    @Operation(summary="Get all available auction houses")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned action houses list with success",
                    content = {
                            @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(anyOf = AuctionHouse.class))
                    })
    })
    public List<AuctionHouse> listAllAuctionHouses(){
        return StreamSupport.stream(auctionHouseRepository.findAll().spliterator(),false).collect(Collectors.toList());
    }


    @DeleteMapping("/{auctionHouseName}")
    @Operation(summary="Delete an auction house")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Auction house deleted with success"),
            @ApiResponse(responseCode = "404", description = "Auction house not found")
    })
    public void deleteAuctionHouse(@PathVariable String auctionHouseName){
        try {
            auctionHouseRepository.deleteById(auctionHouseName);
        } catch (EmptyResultDataAccessException exception){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Auction house [%s] not found", auctionHouseName));
        }
    }

}
